package com.surebrz.sure;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class MainActivity extends ActionBarActivity {
	PlaceholderFragment pf = null;
	ProgressBar pb = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 清除缓存
		deleteFilesByDirectory(this.getCacheDir());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
//		ActionBar bar = this.getActionBar();
//		bar.setLogo(null);
//		bar.hide();
		pf = new PlaceholderFragment();
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, pf).commit();
		}
	}
	
    private void deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                item.delete();
            }
        }
    }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && pf.getWv().canGoBack()) {
			pf.getWv().goBack();// 返回前一个页面
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public class PlaceholderFragment extends Fragment {
		final MainActivity outerContext;
		WebView wv;

		private class WebViewClientDemo extends WebViewClient {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				String title = view.getTitle();
				PlaceholderFragment.this.outerContext.setTitle(title);
			}

			@Override
			// 在WebView中而不是默认浏览器中显示页面
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
			
		}

		private class WebChromeClientExt extends WebChromeClient {
			
			// 标题监控
			@Override
			public void onReceivedTitle(WebView view, String title) {
				super.onReceivedTitle(view, title);
				PlaceholderFragment.this.outerContext.setTitle(title);
			}
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
		        pb.setProgress(newProgress);  
		        if(newProgress==100){  
		            pb.setVisibility(View.GONE);  
		        } else {
		        	pb.setVisibility(View.VISIBLE);
		        }
				super.onProgressChanged(view, newProgress);
			}
		}

		public PlaceholderFragment() {
			this.outerContext = MainActivity.this;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			final View contentView = rootView.findViewById(R.id.webview);

		    pb = (ProgressBar) rootView.findViewById(R.id.pb);
		    pb.setMax(100);

			wv = (WebView) contentView;
			wv.getSettings().setJavaScriptEnabled(true);
			try {
				// wv.loadUrl("http://app.pltshop.com/");
				wv.loadUrl("http://app.pltshop.com/");
			} catch (Exception e) {
				Toast.makeText(this.getActivity(), "cannot connect to server",
						Toast.LENGTH_SHORT).show();
			}
			wv.setWebChromeClient(new WebChromeClientExt());
			wv.setWebViewClient(new WebViewClientDemo());
			return rootView;
		}

		public WebView getWv() {
			return wv;
		}

		public void setWv(WebView wv) {
			this.wv = wv;
		}

		public MainActivity getOuterContext() {
			return outerContext;
		}
	}

}
